

@extends('layouts.app')

@section('content')
<div class="text-center">
    <img src="https://laravelnews.imgix.net/images/laravel-featured.png" class="w-50">
    <h2>Featured Post</h2>
        @foreach($posts as $post)
            <div class="card text-center">
                <div class="card-body">
                    <h4 class="card-tittle mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3 text-muted">Author: {{$post->user->name}}</h6>
                </div>
        </div>
        @endforeach
    
    </div>
@endsection